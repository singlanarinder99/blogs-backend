const express = require("express");
const router = express.Router();
const authenticateToken = require("../authMiddleware");
const bcrypt = require("bcrypt");
const { User, Blog } = require("../models");
const jwt = require("jsonwebtoken");
const multer = require("multer");
require("dotenv").config();

const JWT_SECRET = process.env.JWT_SECRET;

// multer configuration
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// create user api
router.post("/users", upload.single("profilePicture"), async (req, res) => {
  try {
    const { name, email, password } = req.body;
    let profilePicture = null; //

    if (req.file) {
      profilePicture = req.file.buffer.toString("base64");
    }

    // check duplicate email
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res
        .status(400)
        .json({ error: "Email address is already registered" });
    }

    const newUser = new User({ name, email, password, profilePicture });
    await newUser.save();
    res.status(201).json({ msg: "User created successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// login api
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });
    if (!user) {
      return res.status(401).json({ error: "Invalid credentials" });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ error: "Invalid credentials" });
    }

    const token = jwt.sign(
      {
        userId: user._id,
      },
      JWT_SECRET,
      { expiresIn: "24h" }
    );

    const userData = {
      id: user._id,
      name: user.name,
      email: user.email,
      profilePicture: user.profilePicture,
    };

    res.json({ token, ...userData });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// create blog
router.post("/blogs", authenticateToken, async (req, res) => {
  const { heading, body } = req.body;
  const { _id: author } = req.user;

  try {
    const blog = new Blog({ heading, body, author });
    await blog.save();
    res.status(201).json({ message: "Blog created successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

// edit blog
router.put("/blog/:id", authenticateToken, async (req, res) => {
  const { id } = req.params;
  const { heading, body } = req.body;
  const { userId } = req.user;

  try {
    const blog = await Blog.findById(id);
    if (!blog) {
      return res.status(404).json({ error: "Blog not found" });
    }
    // if (blog.author.toString() !== userId) {
    //   return res
    //     .status(403)
    //     .json({ error: "You are not authorized to edit this blog" });
    // }

    blog.heading = heading;
    blog.body = body;
    await blog.save();

    res.json({ message: "Blog updated successfully", blog });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// get blogs api
router.get("/blogs", authenticateToken, async (req, res) => {
  try {
    const blogs = await Blog.find().populate("author").sort({ createdAt: -1 });
    res.json(blogs);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// get user api on the behalf of jwt
router.get("/user", authenticateToken, async (req, res) => {
  try {
    const userId = req.user;
    console.log("userId", userId);
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    const userData = {
      id: user._id,
      name: user.name,
      email: user.email,
      profilePicture: user.profilePicture,
    };
    res.json(userData);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
